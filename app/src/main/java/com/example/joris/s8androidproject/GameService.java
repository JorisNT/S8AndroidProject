package com.example.joris.s8androidproject;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GameService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_GET_ALL_GAMES = "com.example.joris.s8androidproject.action.GET_ALL_GAMES";

    public GameService() {
        super("GameService");
    }

    public static void startActionGetAllCurrencies(Context context) {
        Intent intent = new Intent(context, GameService.class);
        intent.setAction(ACTION_GET_ALL_GAMES);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_ALL_GAMES.equals(action)) {
                this.handleAllGames();
                /*final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);*/
            }
        }
    }

    private void handleAllGames() {
        URL url = null;
        try {
            url = new URL("https://api-2445582011268.apicast.io/games/3,16,10,27,15,35,68,64,78");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("user-key", "3aee928b8e9ccd4ad4be72d2cee7b7a3");
            conn.setRequestMethod("GET");
            conn.connect();
            if(HttpURLConnection.HTTP_OK == conn.getResponseCode()){
                this.copyInputStreamToFile(conn.getInputStream(), new File(getCacheDir(),"games.json"));
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(MainActivity.GAMES_UPDATE));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void copyInputStreamToFile(InputStream in, File file){
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
