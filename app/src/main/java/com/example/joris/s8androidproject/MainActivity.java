package com.example.joris.s8androidproject;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.BitmapFactory;
import android.app.NotificationManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    public static final int NOTIFICATION_ID = 1;
    public static final String GAMES_UPDATE = "GAMES_UPDATE";

    private Button buttonList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        GameService.startActionGetAllCurrencies(this);
        IntentFilter intentFilter = new IntentFilter(GAMES_UPDATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(new GameUpdate(this, MainActivity.this),intentFilter);

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_game);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(new GamesAdapter(GameProvider.getGamesFromFile(this)));

    }

    @Override
    public void onResume()
    {
        super.onResume();
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_game);
        rv.setAdapter(new GamesAdapter(GameProvider.getGamesFromFile(this)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.button_menu_go_favs:
                Intent toFavs = new Intent(MainActivity.this, FavsActivity.class);
                startActivity(toFavs);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void Toast(Context c, String msg) {
        Toast.makeText(c, msg,Toast.LENGTH_LONG).show();
    }

    public static void sendNotification(String title, String content, Context c) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(c);

        builder.setSmallIcon(R.drawable.games);
        builder.setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.games));
        builder.setContentTitle(title);
        builder.setContentText(content);

        NotificationManager notificationManager = (NotificationManager) c.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    private class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.GameHolder> {
        private JSONArray games = null;

        public GamesAdapter(JSONArray games) {
            this.games = games;
        }
        @Override
        public GameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.rv_game_element, parent, false);
            return new GameHolder(view);
        }

        @Override
        public void onBindViewHolder(final GameHolder holder, final int position) {

            try {
                JSONObject data = this.games.getJSONObject(position);

                String name = data.getString("name");
                JSONObject coverObj = data.getJSONObject("cover");
                JSONObject esrbObj = data.getJSONObject("esrb");
                int rating = esrbObj.getInt("rating");
                String cover = coverObj.getString("url").replaceAll("t_thumb", "t_original");
                if (position == 1)
                    Log.d(TAG, "URL COVER GAME : " + cover);
                holder.fullname.setText(name);
                holder.rating.setText("("+ rating + "/10)");
                Glide.with(MainActivity.this).load("https:" + cover)
                    .centerCrop()
                    .into(holder.gameCover);

                holder.buttonDetails.setText(getString(R.string.view_game));
                holder.buttonDetails.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent toDetail = new Intent(MainActivity.this, DetailActivity.class);
                        toDetail.putExtra("position", position);
                        startActivity(toDetail);
                    }
                });

                ArrayList<String> theFavs = GameProvider.getFavs(MainActivity.this);
                if (position == 1) {
                    Log.d(TAG, "fAV GAMES : " + theFavs.size());
                }

                if (theFavs.contains(String.valueOf(position))) {
                    holder.buttonFav.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                }

                holder.buttonFav.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ArrayList<String> favs = GameProvider.getFavs(MainActivity.this);

                        if (favs.contains(String.valueOf(position))) {
                            MainActivity.Toast(MainActivity.this, MainActivity.this.getString(R.string.fav_status_remove));
                            favs.remove(String.valueOf(position));
                            holder.buttonFav.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_off, 0, 0, 0);
                        }
                        else {
                            favs.add(String.valueOf(position));
                            MainActivity.Toast(MainActivity.this, MainActivity.this.getString(R.string.fav_status_add));
                            holder.buttonFav.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.star_on, 0, 0, 0);
                        }

                        GameProvider.setFavs(MainActivity.this, favs);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return this.games.length();
        }

        public class GameHolder extends  RecyclerView.ViewHolder{
            public TextView fullname = null;
            public TextView rating = null;
            public Button buttonDetails = null;
            public Button buttonFav = null;
            public ImageView gameCover = null;

            public GameHolder(View itemView) {
                super(itemView);
                this.fullname = itemView.findViewById(R.id.game_fullname);
                this.rating = itemView.findViewById(R.id.game_rating);
                this.buttonDetails = itemView.findViewById(R.id.country_button_details);
                this.gameCover = itemView.findViewById(R.id.game_cover);
                this.buttonFav = itemView.findViewById(R.id.button_fav);
            }
        }
    }
}
