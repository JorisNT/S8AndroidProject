package com.example.joris.s8androidproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


/**
 * Created by Joris on 29/01/2018.
 */

public class GameUpdate extends BroadcastReceiver {
    private final Context context;
    private final MainActivity main;
    public GameUpdate(Context c, MainActivity main) {
        this.context = c;
        this.main = main;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        this.main.onResume();
        MainActivity.sendNotification(this.context.getString(R.string.games_downloaded_status), this.context.getString(R.string.games_downloaded), this.context);
    }
}
