package com.example.joris.s8androidproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.ContentValues.TAG;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView website_label = (TextView) findViewById(R.id.detail_website_label);
        TextView summary_label = (TextView) findViewById(R.id.detail_summary_label);
        website_label.setText(getString(R.string.website));
        summary_label.setText(getString(R.string.summary));

        TextView title = (TextView) findViewById(R.id.detail_game_name);
        TextView see_more_value = (TextView) findViewById(R.id.detail_see_more_value);
        TextView summary_value = (TextView) findViewById(R.id.detail_summary_value);
        ImageView ImgViewCover = (ImageView) findViewById(R.id.detail_cover);

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        try {
            JSONObject game = GameProvider.getGamesFromFile(this).getJSONObject(position);
            title.setText(game.getString("name"));

            JSONObject coverObj = game.getJSONObject("cover");
            String cover = coverObj.getString("url").replaceAll("t_thumb", "t_original");

            String urlWebsite = game.getString("url");
            see_more_value.setText(urlWebsite);

            String summary = game.getString("summary");
            summary_value.setText(summary);

            JSONArray screenshots = game.getJSONArray("screenshots");
            int [] screenshotIds = new int[]{R.id.detail_screenshot1, R.id.detail_screenshot2, R.id.detail_screenshot3};
            for (int j = 0; j < screenshotIds.length; j++) {
                ImageView screenshotView = (ImageView) findViewById(screenshotIds[j]);
                JSONObject screenshotData = screenshots.getJSONObject(j);
                if (screenshotData != null) {
                    String urlScreenshot = screenshotData.getString("url").replaceAll("t_thumb", "t_original");
                    Glide.with(this).load("https:" + urlScreenshot)
                        .centerCrop()
                        .into(screenshotView);
                }
            }
            Log.d(TAG, "DETAILS URL COVER GAME : " + cover);
            Glide.with(this).load("https:" + cover)
                .centerCrop()
                .into(ImgViewCover);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
