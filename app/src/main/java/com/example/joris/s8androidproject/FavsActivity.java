package com.example.joris.s8androidproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static android.content.ContentValues.TAG;

public class FavsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        getSupportActionBar().setTitle(getString(R.string.fav_title));

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv_fav);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(new FavsActivity.GamesFavsAdapter(GameProvider.getFavsData(this)));
    }

    private class GamesFavsAdapter extends RecyclerView.Adapter<FavsActivity.GamesFavsAdapter.GameFavsHolder> {
        private final JSONArray dataGames;
        private final HashMap<Integer, Integer> gameIds = new HashMap<Integer, Integer>();

        public GamesFavsAdapter(JSONArray games) {
            dataGames = games;
        }

        @Override
        public FavsActivity.GamesFavsAdapter.GameFavsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.rv_game_fav, parent, false);
            return new FavsActivity.GamesFavsAdapter.GameFavsHolder(view);
        }

        @Override
        public void onBindViewHolder(final FavsActivity.GamesFavsAdapter.GameFavsHolder holder, final int position) {
            try {
                JSONObject data = dataGames.getJSONObject(position);

                int id = data.getInt("id");
                this.gameIds.put(position, id);
                String name = data.getString("name");
                JSONObject coverObj = data.getJSONObject("cover");
                JSONObject esrbObj = data.getJSONObject("esrb");
                int rating = esrbObj.getInt("rating");
                String cover = coverObj.getString("url").replaceAll("t_thumb", "t_original");

                holder.fullname.setText(name);
                holder.rating.setText("("+ rating + "/10)");
                Glide.with(FavsActivity.this).load("https:" + cover)
                        .centerCrop()
                        .into(holder.gameCover);

                holder.buttonRemoveFav.setText(getString(R.string.remove));
                holder.buttonRemoveFav.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(FavsActivity.this);

                        builder
                                .setMessage(getString(R.string.fav_question_remove))
                                .setPositiveButton(getString(R.string.answer_yes),  new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {
                                        GameProvider.removeFromFavs(FavsActivity.this, gameIds.get(position));
                                        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_fav);
                                        dataGames.remove(position);
                                        recyclerView.removeViewAt(position);
                                        recyclerView.getAdapter().notifyItemRemoved(position);
                                        recyclerView.getAdapter().notifyItemRangeChanged(position, dataGames.length());
                                        recyclerView.getAdapter().notifyDataSetChanged();
                                    }
                                })
                                .setNegativeButton(getString(R.string.answer_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                })
                                .show();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return dataGames.length();
        }

        public class GameFavsHolder extends  RecyclerView.ViewHolder{
            public TextView fullname = null;
            public TextView rating = null;
            public ImageView gameCover = null;
            public Button buttonRemoveFav = null;

            public GameFavsHolder(View itemView) {
                super(itemView);
                this.fullname = itemView.findViewById(R.id.game_fullname);
                this.rating = itemView.findViewById(R.id.game_rating);
                this.gameCover = itemView.findViewById(R.id.game_cover);
                this.buttonRemoveFav = itemView.findViewById(R.id.fav_button_remove);
            }
        }
    }
}
