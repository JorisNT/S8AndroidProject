package com.example.joris.s8androidproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.ArraySet;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static android.content.ContentValues.TAG;

/**
 * Created by Joris on 29/01/2018.
 */

public class GameProvider {
    public final static String FAVORITE_GAMES = "FAVORITE_GAMES1";
    public final static String PREF_GAMES = "PREF_GAMES";
    public static JSONArray getGamesFromFile(Context context){
        try {
            InputStream is = new FileInputStream(context.getCacheDir() + "/" + "games.json");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            return new JSONArray(new String(buffer, "UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

    public static void setFavs(Context context, ArrayList<String> favs) {
        SharedPreferences settings = context.getSharedPreferences(PREF_GAMES, 0);
        SharedPreferences.Editor editor = settings.edit();
        String result = "";
        for (int i = 0; i < favs.size(); i++) {
            if (i == favs.size() - 1)
                result += String.valueOf(favs.get(i));
            else
                result += (String.valueOf(favs.get(i)) + ",");
        }

        editor.putString(FAVORITE_GAMES, result);
        editor.commit();
    }
    public static ArrayList<String> getFavs(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREF_GAMES, 0);
        String data = settings.getString(FAVORITE_GAMES, "");
        if (data.equals(""))
            return new ArrayList<String>();

        String [] list = data.split(",");
        ArrayList<String> favs = new ArrayList<String>();
        for(int i = 0; i < list.length; i++) {
            favs.add(list[i]);
        }
        return favs;
    }

    public static JSONArray getFavsData(Context context) {
        JSONArray gamesData = getGamesFromFile(context);
        ArrayList<String> favsPos = getFavs(context);

        JSONArray result = new JSONArray();
        for (int i = 0; i < favsPos.size(); i++) {
            try {
                JSONObject data = gamesData.getJSONObject(Integer.parseInt(favsPos.get(i)));
                result.put(data);
            } catch (JSONException e) {
                e.printStackTrace();
                return new JSONArray();
            }
        }
        return result;
    }

    public static void removeFromFavs(Context context, int id) {
        JSONArray gamesData = getGamesFromFile(context);
        ArrayList<String> favsPos = getFavs(context);
        try {
            for (int i = 0; i < gamesData.length(); i++) {
                JSONObject data = gamesData.getJSONObject(i);
                int tId = data.getInt("id");
                if (tId == id) {
                    favsPos.remove(String.valueOf(i));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setFavs(context, favsPos);

    }
}
